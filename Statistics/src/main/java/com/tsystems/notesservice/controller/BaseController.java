package com.tsystems.notesservice.controller;

import com.tsystems.notesservice.domain.Note;
import com.tsystems.notesservice.domain.User;
import com.tsystems.notesservice.service.NoteService;
import com.tsystems.notesservice.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Andrew on 20.07.2014.
 */
@Controller
@RequestMapping("")
public class BaseController {
    protected final Logger logger = Logger.getLogger(getClass());

    @Autowired
    private UserService userService;

    @Autowired
    private NoteService notesService;

    @RequestMapping(value="")
    public String welcome(ModelMap model) {

        logger.info("Request for welcome()");

        return "statistics/welcome";
    }

    @RequestMapping(value = "/notescount/total", method = RequestMethod.POST)
    public String getTotalCountOfNotes(@RequestParam(value = "user_name", required = false) String userName,
                                       Model model) {

        logger.info("Request for getTotalCountOfNotes()");

        List<Note> notes;

        if (userName == null || userName.length() == 0) {
            notes = notesService.getAll();
            model.addAttribute("successMessage", "Number of all notes: " + notes.size());
            model.addAttribute("notesList", notes);
        } else {
            User user;
            user = userService.get(userName);

            if (user != null) {
                notes = user.getNotes();
                model.addAttribute("successMessage", "Number of notes for user with name {" + userName + "}: " + notes.size());
                model.addAttribute("notesList", notes);
            } else {
                model.addAttribute("errorMessage", "User with name {" + userName + "} not exist");
            }
        }

        return "statistics/list";
    }

    @RequestMapping(value = "/notescount/time", method = RequestMethod.POST)
    public String getCountOfNotesForTime(@RequestParam(value = "user", required = false) String userName,
                                         @RequestParam("begin") String beginDate,
                                         @RequestParam("end") String endDate,
                                         Model model) {

        logger.info("Request for getCountOfNotesForTime()");

        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");

        Date parsedBeginDate;
        Date parsedEndDate;

        try {
            parsedBeginDate = sf.parse(beginDate);
            parsedEndDate = sf.parse(endDate);
        } catch (ParseException exception) {
            model.addAttribute("errorMessage", "Date format is incorrect, expected pattern: YYYY-MM-DD");
            return "statistics/welcome";
        }

        List<Note> notes;

        if (userName == null || userName.length() == 0) {
            notes = notesService.getByDate(parsedBeginDate,parsedEndDate);
            model.addAttribute("successMessage", "Number of all notes: " + notes.size());
            model.addAttribute("notesList", notes);
        } else {
            User user;
            user = userService.get(userName);

            if (user != null) {
                notes = notesService.getByDateAndUserName(parsedBeginDate,parsedEndDate,user.getName());
                model.addAttribute("successMessage", "Number of notes for user {" + user.toString() + "}: " + notes.size());
                model.addAttribute("notesList", notes);
            } else {
                model.addAttribute("errorMessage", "User with name {" + userName + "} not exist");
            }
        }

        return "statistics/list";
    }

    @RequestMapping(value = "/useractivity", method = RequestMethod.POST)
    public String getMaxUserActivity(@RequestParam(value = "name", required = false) String userName,
                                         Model model) {

        logger.info("Request for getMaxUserActivity()");

        if (userName != null && userName.length() != 0) {
            User user;
            user = userService.get(userName);

            if (user != null) {
                Map.Entry<String, Integer> maxActivity;
                maxActivity = notesService.getMaxUserActivity(user);

                if (maxActivity != null) {
                    model.addAttribute("successMessage", "Date of maximum user {" + user.toString() + "} activity: "
                            + maxActivity.getKey() + ", count of notes during the day: " + maxActivity.getValue());

                    return "statistics/welcome";
                } else {
                    model.addAttribute("successMessage", "User {\" + user.toString() + \"} has not added any notes");
                    return "statistics/welcome";
                }
            }
            model.addAttribute("errorMessage", "User with name {" + userName + "} not exist");
        } else {
            model.addAttribute("errorMessage", "User name is empty");
        }

        return "statistics/welcome";
    }
}
