<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 25.07.2014
  Time: 20:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Adding new note</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/messagesStyle.css" media="all">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/dataBlock.css" media="all">
    <style type="text/css">
        td, th {
            padding: 7px 10px 5px 10px;
        }
    </style>
</head>
<body>
    <div style="text-align: center;">
        <%@ include file="/WEB-INF/pages/header.jsp" %>
        <c:if test="${!empty successMessage}">
            <form:form cssClass="successBlock" element="div" id="successBlock">
                ${successMessage}
            </form:form>
        </c:if>

        <c:if test="${!empty errorMessage}">
            <form:form cssClass="errorBlock" element="div" id="errorBlock">
                ${errorMessage}
            </form:form>
        </c:if>

        <h2 style="margin-top: 30px">Statistics page</h2>

        <h3 style="margin-top: 30px">Total of user notes:</h3>
        <form method="post" action="${pageContext.request.contextPath}/notescount/total" name="search_query">
            <table align="center" >
                <tr>
                    <td style="text-align: left;">Name of user:</td>
                    <td><input type="text" id="user_name" name="user_name" style="width: 250px;"  placeholder="Leave empty for hold for all"/></td>
                </tr>
                <tr>
                    <td/>
                    <td style="text-align: right;"><input type="submit" value="Show"/></td>
                </tr>
            </table>
        </form>

        <h3 style="margin-top: 50px">Total of user notes for the period:</h3>
        <form method="post" action="${pageContext.request.contextPath}/notescount/time" name="search_query">
            <table align="center" >
                <tr>
                    <td style="text-align: left;">Name of user:</td>
                    <td><input type="text" id="user" name="user" style="width: 250px;"  placeholder="Leave empty for hold for all"/></td>
                </tr>
                <tr>
                    <td style="text-align: left;">Begin date:</td>
                    <td><input type="date" id="begin" name="begin" style="width: 250px;" placeholder="YYYY-MM-DD"/></td>
                </tr>
                <tr>
                    <td style="text-align: left;">End date:</td>
                    <td><input type="date" id="end" name="end" style="width: 250px;"  placeholder="YYYY-MM-DD"/></td>
                </tr>
                <tr>
                    <td/>
                    <td style="text-align: right;"><input type="submit" value="Show"/></td>
                </tr>
            </table>
        </form>

        <h3 style="margin-top: 50px">Search for date of maximum user activity:</h3>
        <form method="post" action="${pageContext.request.contextPath}/useractivity" name="search_query">
            <table align="center" >
                <tr>
                    <td style="text-align: left;">Name of user:</td>
                    <td><input type="text" id="name" name="name" style="width: 250px;"  placeholder="User name, ofc.."/></td>
                </tr>
                <tr>
                    <td/>
                    <td style="text-align: right;"><input type="submit" value="Show"/></td>
                </tr>
            </table>
        </form>
</body>
</html>
