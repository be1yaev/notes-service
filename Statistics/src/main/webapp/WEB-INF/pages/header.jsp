<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 27.07.2014
  Time: 4:59
  To change this template use File | Settings | File Templates.
--%>
<div style="margin-top: 20px; margin-bottom: 30px; text-shadow: 0px 2px 3px #999; font-size: 20px; font-family:Tahoma;">
    Pages:
    <a href="/NotesService">Welcome</a> |
    <a href="/NotesService/user">List of users</a> |
    <a href="/NotesService/note">List of notes</a> |
    <a href="/NotesService/note/add">Add new note</a> |
    <a href="/NotesService/note/search">Search notes</a> |
    <a href="${pageContext.request.contextPath}/">Statistics</a>
</div>
