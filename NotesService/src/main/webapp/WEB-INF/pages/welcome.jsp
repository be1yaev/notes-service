<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 25.07.2014
  Time: 20:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <body>
        <div style="text-align: center;">
            <%@ include file="/WEB-INF/pages/header.jsp" %>
            <h2 style="margin: 100px;">
                Hello! This is simple service for CRUD operations.
            </h2>
            <p>
            <h3>
                For starters, you can create a user, and then a note in his name
            </h3>
            </p>
        </div>
    </body>
</html>
