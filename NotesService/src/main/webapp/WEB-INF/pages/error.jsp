<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 25.07.2014
  Time: 21:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <title>Error page</title>
</head>
<body>
<div style="text-align: center; font-size: 23px;">
    <%@ include file="/WEB-INF/pages/header.jsp" %>
    <h1 style="margin-top: 50px;">
        Error ${errorCode}<c:if test="${empty errorCode}">404</c:if>
    </h1>
    <p style="margin-top: 50px; color: #222; text-shadow: 0px 2px 3px #555; font-size: 100px; font-family:Tahoma;">
        =(
    </p>
    <c:if test="${!empty errorMessage}">
        <h2 style="margin: 50px;">
            Description:
        </h2>
        ${errorMessage}
    </c:if>
    <!-- StackTrace (if exist):
        ${exceptionStackTrace}
    -->
</div>
</body>
</html>