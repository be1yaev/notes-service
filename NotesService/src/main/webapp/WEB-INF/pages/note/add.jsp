<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 26.07.2014
  Time: 4:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Adding new note</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/messagesStyle.css" media="all">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/dataBlock.css" media="all">
    <style type="text/css">
        td, th {
            padding: 7px 10px 5px 10px;
        }
    </style>
</head>
<body>
<div style="text-align: center;">
    <%@ include file="/WEB-INF/pages/header.jsp" %>
    <c:if test="${!empty successMessage}">
        <form:form cssClass="successBlock" element="div" id="successBlock">
            ${successMessage}
        </form:form>
    </c:if>

    <c:if test="${!empty errorMessage}">
        <form:form cssClass="errorBlock" element="div" id="errorBlock">
            ${errorMessage}
        </form:form>
    </c:if>

    <h3 style="margin-top: 30px">Complete the fields:</h3>
    <form:form method="post" action="${pageContext.request.contextPath}/note/confirm/add" commandName="note">
        <form:errors path="*" cssClass="errorBlock" element="div"/>
        <table align="center" >
            <%--<tr>--%>
                <%--<td style="text-align: left;">Note ID:</td>--%>
                <%--<td><form:input path="id" type="number" placeholder="Must be empty for adding" cssErrorClass="error"/></td>--%>
            <%--</tr>--%>
            <tr>
                <td style="text-align: right;">Header:</td>
                <td><form:input path="header" cssStyle="width: 250px;" placeholder="Input short description.." cssErrorClass="error"/></td>
            </tr>
            <tr>
                <td style="text-align: right;">Text:</td>
                <td><form:textarea path="text" cssStyle="width: 250px;" rows="5" cols="30" placeholder="Input your text here.." cssErrorClass="error"/></td>
            </tr>
            <tr>
                <td style="text-align: right;">Author (User name):</td>
                <td><form:input path="user.name" cssStyle="width: 250px;" placeholder="This user must be exist" cssErrorClass="error"/></td>
            </tr>
            <tr>
                <td/>
                <td style="text-align: right;"><input type="submit" value="Add"/></td>
            </tr>
        </table>
    </form:form>
</div>
</body>
</html>
