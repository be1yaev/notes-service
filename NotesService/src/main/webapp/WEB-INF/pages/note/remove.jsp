<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 26.07.2014
  Time: 4:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Remove note</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/messagesStyle.css" media="all">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/dataBlock.css" media="all">
    <style type="text/css">
        td, th {
            padding: 7px 10px 5px 10px;
        }
    </style>
</head>
<body>
<div style="text-align: center;">
    <%@ include file="/WEB-INF/pages/header.jsp" %>

<%--<c:if test="${!empty successMessage}">--%>
    <%--<form:form cssClass="successBlock" element="div" id="successBlock">--%>
        <%--${successMessage}--%>
    <%--</form:form>--%>
<%--</c:if>--%>

<%--<c:if test="${!empty errorMessage}">--%>
    <%--<form:form cssClass="errorBlock" element="div" id="errorBlock">--%>
        <%--${errorMessage}--%>
    <%--</form:form>--%>
<%--</c:if>--%>

<h3 style="margin-top: 30px">Do you really want to delete the following note?</h3>
    <table align="center" >
        <tr>
            <td style="text-align: left;">Note ID:</td>
            <td>${note.id}</td>
        </tr>
        <tr>
            <td style="text-align: left;">Header:</td>
            <td>${note.header}</td>
        </tr>
        <tr>
            <td style="text-align: left;">Text:</td>
            <td>${note.text}</td>
        </tr>
        <tr>
            <td style="text-align: left;">Author (User name):</td>
            <td>${note.user.name}</td>
        </tr>
        <tr>
            <td/>
            <td style="text-align: right;"><input type="button" value="Remove" onclick="location.href='${pageContext.request.contextPath}/note/confirm/remove/${note.id}'"/></td>
        </tr>
    </table>
</div>
</body>
</html>
