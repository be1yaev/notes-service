<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 25.07.2014
  Time: 20:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Notes</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/messagesStyle.css" media="all">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/dataBlock.css" media="all">
</head>
<body>
<div style="text-align: center;">
    <%@ include file="/WEB-INF/pages/header.jsp" %>
    <h2>Notes page</h2>

    <c:if test="${!empty successMessage}">
        <form:form cssClass="successBlock" element="div" id="successBlock">
            ${successMessage}
        </form:form>
    </c:if>

    <c:if test="${!empty errorMessage}">
        <form:form cssClass="errorBlock" element="div" id="errorBlock">
            ${errorMessage}
        </form:form>
    </c:if>

    <c:choose>
        <c:when test="${!empty notesList}">
            <h3 style="margin-top: 20px">Notes list:</h3>
            <div class="dataBlock">
                <table class="data" align="center">
                    <tr>
                        <th>Id</th>
                        <th>Date and time</th>
                        <th>Author</th>
                        <th>Header</th>
                        <th>Text</th>
                        <th>Operations</th>
                    </tr>
                    <c:forEach items="${notesList}" var="note">
                        <tr>
                            <td>${note.id}</td>
                            <td>${note.date}</td>
                            <td>${note.user.name}</td>
                            <td>${note.header}</td>
                            <td>${note.text}</td>
                            <td>
                                <a href="${pageContext.request.contextPath}/note/remove/${note.id}">remove</a>
                                |
                                <a href="${pageContext.request.contextPath}/note/edit/${note.id}">edit</a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </c:when>
        <c:otherwise>
            <h3 style="margin-top: 10px">List of notes is empty</h3>
        </c:otherwise>
    </c:choose>
</div>
</body>
</html>
