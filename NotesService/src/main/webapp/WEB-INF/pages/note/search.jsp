<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 29.07.2014
  Time: 4:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Search</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/messagesStyle.css" media="all">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/dataBlock.css" media="all">
    <style type="text/css">
        td, th {
            padding: 7px 10px 5px 10px;
        }
    </style>
</head>
<body>
<div style="text-align: center;">
    <%@ include file="/WEB-INF/pages/header.jsp" %>
    <c:if test="${!empty successMessage}">
        <form:form cssClass="successBlock" element="div" id="successBlock">
            ${successMessage}
        </form:form>
    </c:if>

    <c:if test="${!empty errorMessage}">
        <form:form cssClass="errorBlock" element="div" id="errorBlock">
            ${errorMessage}
        </form:form>
    </c:if>

    <h3 style="margin-top: 30px">Complete the fields:</h3>
    <form method="post" action="${pageContext.request.contextPath}/note/search/find" name="search_query">
        <table align="center" >
            <tr>
                <td style="text-align: left;">Begin date:</td>
                <td><input type="date" id="begin" name="begin" style="width: 250px;" placeholder="YYYY-MM-DD"/></td>
            </tr>
            <tr>
                <td style="text-align: left;">End date:</td>
                <td><input type="date" id="end" name="end" style="width: 250px;"  placeholder="YYYY-MM-DD"/></td>
            </tr>
            <tr>
                <td style="text-align: left;">Header:</td>
                <td><input type="text" id="header" name="header" style="width: 250px;"  placeholder="Input full or part of header.."/></td>
            </tr>
            <tr>
                <td style="text-align: left;">Check it, if you need exact match:</td>
                <td style="text-align: left;"><input type="checkbox" id="exact_match" name="exact_match" style="width: 250px;" /></td>
            </tr>
            <tr>
                <td/>
                <td style="text-align: right;"><input type="submit" value="Search"/></td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>
