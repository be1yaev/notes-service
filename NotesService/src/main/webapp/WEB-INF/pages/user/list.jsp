<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 25.07.2014
  Time: 20:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Users</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/messagesStyle.css" media="all">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/dataBlock.css" media="all">
</head>
<body>
<div style="text-align: center; margin-top: 10px">
    <%@ include file="/WEB-INF/pages/header.jsp" %>
    <h2>Users page</h2>

    <c:if test="${!empty successMessage}">
        <form:form cssClass="successBlock" element="div" id="successBlock">
            ${successMessage}
        </form:form>
    </c:if>

    <c:if test="${!empty errorMessage}">
        <form:form cssClass="errorBlock" element="div" id="errorBlock">
            ${errorMessage}
        </form:form>
    </c:if>

    <h3 style="margin-top: 20px">Adding or updating user:</h3>
    <form:form method="post" action="${pageContext.request.contextPath}/user/update" commandName="user">
        <form:errors path="*" cssClass="errorBlock" element="div"/>
        <table align="center" >
            <tr>
                <td style="text-align: left;">User ID:</td>
                <td><form:input path="id" type="number" placeholder="Must be empty for adding" cssErrorClass="error"/></td>
            </tr>
            <tr>
                <td style="text-align: left;">Name of User:</td>
                <td><form:input path="name" cssErrorClass="error"/></td>
            </tr>
            <tr>
                <td/>
                <td style="text-align: right;"><input type="submit" value="Save"/></td>
            </tr>
        </table>
    </form:form>
    <c:choose>
        <c:when test="${!empty usersList}">
            <h3 style="margin-top: 20px">Users list:</h3>
            <div class="dataBlock">
                <table class="data" align="center">
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Operations</th>
                    </tr>
                    <c:forEach items="${usersList}" var="user">
                        <tr>
                            <td>${user.id}</td>
                            <td>${user.name}</td>
                            <td>
                                <a href="${pageContext.request.contextPath}/user/remove/${user.id}">remove</a>
                                 |
                                <a href="${pageContext.request.contextPath}/user/edit/${user.id}">edit</a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </c:when>
        <c:otherwise>
            <h3 style="margin-top: 10px">List of users is empty</h3>
        </c:otherwise>
    </c:choose>
</div>
</body>
</html>
