package com.tsystems.notesservice.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Created by Andrew on 20.07.2014.
 */
@Entity
@Table(name="table_notes")
public class Note implements Comparable {

    @Id
    @Column(name="id")
    @GeneratedValue
    private Long id;

    //@NotEmpty(message = "Field \"UserId\" can not be empty")
    @NotNull(message = "Field \"UserId\" can not be empty")
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="user_id")
    private User user;

    @NotNull(message = "Field \"Header\" can not be empty")
    @Size(min=1, max=50, message = "Length of \"Header\" must be between {2} and {1} characters")
    @Column(name="header")
    private String header;

    @NotNull(message = "Field \"Text\" can not be empty")
    @Size(min=1, max=150, message = "Length of \"Text\" must be between {2} and {1} characters")
    @Column(name="text")
    private String text;

    //@NotNull(message = "Field \"Date\" can not be empty")
    @Column(name="date")
    @Temporal(value=TemporalType.TIMESTAMP)
    private Date date;

    public Note() {}

    public Note(Long id, User user, String header, String text, Date date) {
        this.id = id;
        this.user = user;
        this.header = header;
        this.text = text;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int compareTo(Object obj) {
        return this.date.compareTo(((Note)obj).getDate());
    }

    @Override
    public String toString() {
        return "ID: " + id
                + "; DATE: " + date
                + "; HEADER: " + header
                + "; TEXT: " + text
                + "; USER: " + user.getName();
    }
}
