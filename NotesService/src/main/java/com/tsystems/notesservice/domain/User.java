package com.tsystems.notesservice.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created by Andrew on 20.07.2014.
 */
@Entity
@Table(name="table_users", uniqueConstraints = {@UniqueConstraint(columnNames = "name")})
public class User implements Comparable {

    @Id
    @Column(name="id")
    //@Min(value = 1, message = "Incorrect ID, please input numeric value")
    @GeneratedValue
    private Long id;

    @NotNull(message = "Field \"User name\" can not be empty")
    @Size(min = 3, max = 50, message = "Length of \"User name\" must be between {2} and {1} characters")
    //@NotEmpty(message = "Field \"User name\" can not be empty")
    //@Length(min = 3, max = 50, message = "Length of \"User name\" must be between {2} and {1} characters")
    @Column(name = "name", unique = true)
    private String name;

    @OneToMany(cascade={CascadeType.REMOVE}, mappedBy = "user", fetch=FetchType.EAGER)
    private List<Note> notes;

    public User() {}

    public User(Long id, String name, List<Note> notes) {
        this.id = id;
        this.name = name;
        this.notes = notes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    @Override
    public int compareTo(Object obj) {
        return this.id.compareTo(((User)obj).getId());
    }

    @Override
    public String toString() {
        return "ID: " + id + "; NAME: " + name;
    }
}
