package com.tsystems.notesservice.dao;

import com.tsystems.notesservice.domain.User;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

/**
 * Created by Andrew on 22.07.2014.
 */
@Repository
public class UserDAOImpl implements UserDAO {
    @Autowired
    private SessionFactory sessionFactory;

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public User get(Long id) {
        String sqlQuery =" from User as user where user.id =:id";
        Query query = getCurrentSession().createQuery(sqlQuery);
        query.setString("id", id.toString());

        List<User> users = query.list();

        if(!users.isEmpty())
            return users.get(0);

        return null;
    }

    @Override
    public User get(String name) {
        String sqlQuery =" from User as user where user.name =:name";
        Query query = getCurrentSession().createQuery(sqlQuery);
        query.setString("name", name);

        List<User> users = query.list();

        if(!users.isEmpty())
            return users.get(0);

        return null;
    }

    @Override
    public boolean add(User user) {

        if (get(user.getName()) == null) {
            getCurrentSession().save(user);
            return true;
        }

        return false;
    }

    @Override
    public void update(User user) {
        User userToUpdate = get(user.getId());

        userToUpdate.setName(user.getName());
        userToUpdate.setNotes(user.getNotes());

        getCurrentSession().update(userToUpdate);
    }

    @Override
    public void remove(Long id) {
        User removableUser = get(id);
        if (removableUser != null) {
            getCurrentSession().delete(removableUser);
        }
    }

    @Override
    public List<User> getAll() {
        return (List<User>)getCurrentSession().createQuery("from User").list();
    }
}
