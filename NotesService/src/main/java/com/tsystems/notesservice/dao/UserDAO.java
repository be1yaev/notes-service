package com.tsystems.notesservice.dao;

import com.tsystems.notesservice.domain.User;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Andrew on 22.07.2014.
 */
@Repository
public interface UserDAO {

    public User get(Long id);
    public User get(String name);
    public boolean add(User note);
    public void update(User note);
    public void remove(Long id);
    public List<User> getAll();
}