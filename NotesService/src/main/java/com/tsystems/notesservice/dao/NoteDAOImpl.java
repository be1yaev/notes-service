package com.tsystems.notesservice.dao;

import com.tsystems.notesservice.domain.Note;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Andrew on 21.07.2014.
 */
@Repository
public class NoteDAOImpl implements NoteDAO {
    @Autowired
    private SessionFactory sessionFactory;

//    private static final DateFormat sqlDateAFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
private static final DateFormat sqlDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Note get(Long id) {

        String sqlQuery =" from Note where id =:id";
        Query query = getCurrentSession().createQuery(sqlQuery);
        query.setString("id", id.toString());


        List<Note> notes = query.list();

        if(!notes.isEmpty())
            return notes.get(0);

        return null;
    }

    @Override
    public List<Note> getByDate(Date begin, Date end) {

        // Row fetching that match the specified date range
        String sqlQuery =" from Note where date_format(date,'%Y-%m-%d') >=:begin and date_format(date,'%Y-%m-%d') <=:end";
        Query query = getCurrentSession().createQuery(sqlQuery);
        query.setString("begin", sqlDateFormat.format(begin));
        query.setString("end", sqlDateFormat.format(end));

        return query.list();
    }

    @Override
    public List<Note> getByDateAndHeader(Date begin, Date end, String header, boolean exactMatch) {

        // Escape NullPointerException
        if (header == null)
            header = "";

        // For LIKE query:
        // String sqlQuery =" from Note where date_format(date,'%Y-%m-%d') >=:begin and date_format(date,'%Y-%m-%d') <=:end and note.header like :header";
        // query.setString("header", (exactMatch) ? header : "%" + header + "%");

        List<Note> result;
        result = new ArrayList<>();

        // Adding rows, that do contain the required header, into result array
        // (it's better LIKE query that allows you to use special characters in the title)
        for (Note row : getByDate(begin, end)) {
            if (exactMatch) {
                if (row.getHeader().equalsIgnoreCase(header))
                    result.add(row);
            } else {
                if (row.getHeader().toLowerCase().indexOf(header.toLowerCase()) != -1)
                    result.add(row);
            }
        }

        return result;
    }

    @Override
    public void add(Note note) {

        getCurrentSession().save(note);
    }

    @Override
    public void update(Note note) {

        Note noteToUpdate = get(note.getId());

        noteToUpdate.setHeader(note.getText());
        noteToUpdate.setText(note.getText());
        noteToUpdate.setDate(note.getDate());
        //noteToUpdate.setUser(note.getUser());

        getCurrentSession().update(noteToUpdate);
    }

    @Override
    public void remove(Long id) {

        Note removableNote = get(id);
        if (removableNote != null) {
            getCurrentSession().delete(removableNote);
        }
    }

    @Override
    public List<Note> getAll() {

        return (List<Note>)getCurrentSession().createQuery("from Note").list();
    }
}
