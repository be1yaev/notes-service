package com.tsystems.notesservice.dao;

import com.tsystems.notesservice.domain.Note;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by Andrew on 21.07.2014.
 */
@Repository
public interface NoteDAO {

    public Note get(Long id);
    /**
     * Returns found records between two dates (inclusive) and to specify the header, if specified.
     * If the flag "exactMatch" set, sought partial entry title
     * @param begin smallest date
     * @param end greatest date
     * @param header header or it part
     * @param exactMatch set true for exact search
     * @return records that has been found
     */
    public List<Note> getByDateAndHeader(Date begin, Date end, String header, boolean exactMatch);
    /**
     * Returns found records between two dates (inclusive)
     * @param begin smallest date
     * @param end greatest date
     * @return records that has been found
     */
    public List<Note> getByDate(Date begin, Date end);
    public void add(Note note);
    public void update(Note note);
    public void remove(Long id);
    public List<Note> getAll();
}
