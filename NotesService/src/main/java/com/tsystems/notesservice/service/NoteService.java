package com.tsystems.notesservice.service;

import com.tsystems.notesservice.domain.Note;
import com.tsystems.notesservice.domain.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Andrew on 22.07.2014.
 */
@Service
@Transactional
public interface NoteService {

    public Note get(Long id);

    /**
     * Returns found records between two dates (inclusive), which belong to this user
     * @param begin smallest date
     * @param end greatest date
     * @param userName user name
     * @return records that has been found
     */
    public List<Note> getByDateAndUserName(Date begin, Date end, String userName);

    /**
     * Returns found records between two dates (inclusive) and to specify the title, if specified. If the flag "exactMatch" set, sought partial entry title
     * @param begin smallest date
     * @param end greatest date
     * @param header header or it part
     * @param exactMatch set false for partial entry title
     * @return records that has been found
     */
    public List<Note> getByDateAndHeader(Date begin, Date end, String header, boolean exactMatch);
    /**
     * Returns found records between two dates (inclusive)
     * @param begin smallest date
     * @param end greatest date
     * @return records that has been found
     */
    public List<Note> getByDate(Date begin, Date end);
    public void add(Note note);
    public void update(Note note);
    public void remove(Long id);
    public List<Note> getAll();
    /**
     * Returns max user activity (Date and count of notes during the day)
     * @param user user for search
     * @return max user activity as Map.Entry with Date and notes count in this day
     */
    public Map.Entry<String, Integer> getMaxUserActivity(User user);
}
