package com.tsystems.notesservice.service;

import com.tsystems.notesservice.dao.UserDAO;
import com.tsystems.notesservice.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

/**
 * Created by Andrew on 22.07.2014.
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDAO userDAO;

    @Override
    public User get(Long id) {
        return userDAO.get(id);
    }

    @Override
    public User get(String name) {
        return userDAO.get(name);
    }

    @Override
    public boolean add(User note) {
        return userDAO.add(note);
    }

    @Override
    public void update(User note) {
        userDAO.update(note);
    }

    @Override
    public void remove(Long id) {
        userDAO.remove(id);
    }

    @Override
    public List<User> getAll() {
        List<User> allUsers;
        allUsers = userDAO.getAll();

        Collections.sort(allUsers,Collections.reverseOrder());

        return allUsers;
    }
}