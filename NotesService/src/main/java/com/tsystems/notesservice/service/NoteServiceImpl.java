package com.tsystems.notesservice.service;

import com.tsystems.notesservice.dao.NoteDAO;
import com.tsystems.notesservice.domain.Note;
import com.tsystems.notesservice.domain.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Andrew on 22.07.2014.
 */
@Service
@Transactional
public class NoteServiceImpl implements NoteService {

    protected final Logger logger = Logger.getLogger(getClass());

    @Autowired
    private NoteDAO noteDAO;

    @Override
    public Note get(Long id) {
        return noteDAO.get(id);
    }

    @Override
    public List<Note> getByDate(Date begin,
                                Date end) {

        List<Note> matches;
        matches = noteDAO.getByDate(begin, end);

        Collections.sort(matches, Collections.reverseOrder());

        return matches;
    }

    @Override
    public List<Note> getByDateAndUserName(Date begin,
                                           Date end,
                                           String userName) {

        List<Note> matches;
        matches = noteDAO.getByDate(begin, end);

        // Delete notes, which do not belong to this user
        Note row;
        for (int i = 0; i < matches.size(); ) {
            row = matches.get(i);

            if (!userName.equals(row.getUser().getName())) {
                matches.remove(i);
            } else {
                i++;
            }
        }

        Collections.sort(matches, Collections.reverseOrder());

        return matches;
    }

    @Override
    public List<Note> getByDateAndHeader(Date begin,
                                         Date end,
                                         String header,
                                         boolean exactMatch) {

        List<Note> matches;
        matches = noteDAO.getByDateAndHeader(begin, end, header, exactMatch);

        Collections.sort(matches, Collections.reverseOrder());

        return matches;
    }

    @Override
    public void add(Note note) {
        note.setDate(new Date());
        noteDAO.add(note);
    }

    @Override
    public void update(Note note) {
        note.setDate(new Date());
        noteDAO.update(note);
    }

    @Override
    public void remove(Long id) {
        noteDAO.remove(id);
    }

    @Override
    public List<Note> getAll() {

        List<Note> allNotes;
        allNotes = noteDAO.getAll();

        Collections.sort(allNotes, Collections.reverseOrder());

        return noteDAO.getAll();
    }

    @Override
    public Map.Entry<String, Integer> getMaxUserActivity(User user) {
        TreeMap<String, Integer> map = new TreeMap<>();

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        int previousRepeatsCount; // Contains the number of the current date in loop

        List<Note> userNotes;
        userNotes = user.getNotes();

        if (userNotes.size() == 0)
            return null;

        String date;
        for (Note note : userNotes) {
            date = formatter.format(note.getDate());
            previousRepeatsCount = 0;

            if (map.containsKey(date)) {
                previousRepeatsCount = map.get(date);
                map.remove(date);
            }

            // Adding new row to string or replacing old value of repeats count
            map.put(date, previousRepeatsCount + 1);
        }

        // Creating new container with Map.Entry
        ArrayList<Map.Entry<String, Integer>> sortedArray;
        sortedArray = new ArrayList<>(map.entrySet());

        // Sorting by values
        Collections.sort(sortedArray,
                new Comparator<Map.Entry<String, Integer>>() {
                    @Override
                    public int compare(Map.Entry<String, Integer> first, Map.Entry<String, Integer> second) {
                        return (-1) * first.getValue().compareTo(second.getValue());
                    }
        });

        return sortedArray.get(0);
    }
}
