package com.tsystems.notesservice.service;

import com.tsystems.notesservice.domain.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Andrew on 22.07.2014.
 */
@Service
@Transactional
public interface UserService {

    public User get(Long id);
    public User get(String name);
    public boolean add(User note);
    public void update(User note);
    public void remove(Long id);
    public List<User> getAll();
}