package com.tsystems.notesservice.controller;

import com.tsystems.notesservice.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Andrew on 20.07.2014.
 */
@Controller
@RequestMapping("")
public class BaseController {
    protected final Logger logger = Logger.getLogger(getClass());

    @Autowired
    private UserService userService;

    @RequestMapping(value="")
    public String welcome(ModelMap model) {

        logger.info("Request for welcome()");

        return "welcome";
    }

//
//    @RequestMapping(value="/users")
//    public String usersList(ModelMap model) {
//
//        model.addAttribute("usersList", userService.getAll());
//
//        return "index";
//    }

//    @RequestMapping(value="/welcome", method = RequestMethod.GET)
//    public String welcome2(ModelMap model) {
//        model.addAttribute("message", "Maven Web Project + Spring 3 MVC - welcome()");
//
//        return "index";
//    }
//
//    @RequestMapping(value="/welcome/{name}", method = RequestMethod.GET)
//    public String welcomeName(@PathVariable String name, ModelMap model) {
//        model.addAttribute("message", "Maven Web Project + Spring 3 MVC - " + name);
//
//        return "index";
//    }
}
