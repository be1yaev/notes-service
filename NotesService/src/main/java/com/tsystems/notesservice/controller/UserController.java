package com.tsystems.notesservice.controller;

import com.tsystems.notesservice.domain.User;
import com.tsystems.notesservice.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * Created by Andrew on 25.07.2014.
 */
@Controller
@RequestMapping(value = "/user")
public class UserController {
    protected final Logger logger = Logger.getLogger(getClass());

    @Autowired
    private UserService userService;

    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest req, Exception exception) {

        logger.error("Request: " + req.getRequestURL() + " raised " + exception);

        StringBuffer stackTrace = new StringBuffer(50);
        for (StackTraceElement element : exception.getStackTrace()) {
            stackTrace.append(element.toString() + "\n");
        }

        ModelAndView mav = new ModelAndView();
        mav.addObject("errorMessage", exception);
        mav.addObject("exceptionStackTrace", stackTrace);
        mav.addObject("errorCode", "400");
        mav.setViewName("error");
        return mav;
    }

    @RequestMapping(value="")
    public String usersList(Model model) {

        logger.info("Request for usersList()");

        if (!model.containsAttribute("user"))
            model.addAttribute("user", new User());

        model.addAttribute("usersList", userService.getAll());

        return "user/list";
    }

    @RequestMapping(value="/add", method = RequestMethod.POST)
    public String addNewUser(@Valid @ModelAttribute("user") User user,
                             BindingResult result,
                             Model model) {

        logger.info("Request for addNewUser()");

        if (result.hasErrors()) {
            model.addAttribute("user", user);
            return usersList(model);
        }

        if (userService.add(user)) {
            model.addAttribute("successMessage", "User {" + user.toString() + "} has been successfully added into DB");
        } else {
            model.addAttribute("errorMessage", "User has not added to DB. Name must be unique and correct");
        }
        return usersList(model);
    }

    @RequestMapping("/remove/{id}")
    public String removeUser(@PathVariable("id") Long userId,
                             Model model) {

        logger.info("Request for removeUser()");

        User userToDelete;
        userToDelete = userService.get(userId);

        if (userToDelete != null) {
            String deletedUser = userToDelete.toString();
            userService.remove(userId);
            model.addAttribute("successMessage", "User {" + deletedUser + "} has been successfully removed from DB");
            return usersList(model);
        } else {
            model.addAttribute("errorMessage", "User with {ID:" + userId + "} does not exist");
            return usersList(model);
        }
    }

    @RequestMapping("/edit/{id}")
    public String editUser(@PathVariable("id") Long userId,
                           Model model) {

        logger.info("Request for editUser()");

        User userToEdit;
        userToEdit = userService.get(userId);

        if (userToEdit != null) {
            model.addAttribute("user", userToEdit);
            return usersList(model);
        } else {
            model.addAttribute("errorMessage", "User with {ID:" + userId + "} does not exist");
            return usersList(model);
        }
    }

    @RequestMapping("/update")
    public String updateUser(@Valid @ModelAttribute("user") User user,
                             BindingResult result,
                             Model model) {

        logger.info("Request for updateUser()");

        if (result.hasErrors()) {
            model.addAttribute("user", user);
            return usersList(model);
        }

        if (user.getId() != null) {
            User userToUpdate;
            userToUpdate = userService.get(user.getId());

            if (userToUpdate != null) {
                String lastRecord = userToUpdate.toString();
                userService.update(user);
                model.addAttribute("successMessage",
                        "User information has been successfully updated. " +
                        "Last record: {" + lastRecord + "}, " +
                        "new record: {" + user + "}");
                return usersList(model);
            } else {
                model.addAttribute("errorMessage",
                        "User with {ID:" + user.getId() + "} does not exist. Updating is not possible");
                return usersList(model);
            }
        }

        return addNewUser(user, result, model);
    }
}
