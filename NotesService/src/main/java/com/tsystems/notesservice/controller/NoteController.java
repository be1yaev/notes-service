package com.tsystems.notesservice.controller;

import com.tsystems.notesservice.domain.Note;
import com.tsystems.notesservice.domain.User;
import com.tsystems.notesservice.service.NoteService;
import com.tsystems.notesservice.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Andrew on 25.07.2014.
 */
@Controller
@RequestMapping(value = "/note")
public class NoteController {
    protected final Logger logger = Logger.getLogger(getClass());

    @Autowired
    private NoteService noteService;

    @Autowired
    private UserService userService;

    @InitBinder
    /**
     * Resolving conflicts in date validation.
     * Date format changed to correct format for db.
     */
    public void initBinder(WebDataBinder binder)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(
                dateFormat, true));
    }

    @ExceptionHandler(Exception.class)
    /**
     * Interceptor for unhandled exceptions
     */
    public ModelAndView handleError(HttpServletRequest req, Exception exception) {

        logger.error("Request: " + req.getRequestURL() + " raised " + exception);

        StringBuffer stackTrace = new StringBuffer(50);
        for (StackTraceElement element : exception.getStackTrace()) {
            stackTrace.append(element.toString() + "\n");
        }

        ModelAndView mav = new ModelAndView();
        mav.addObject("errorMessage", exception);
        mav.addObject("exceptionStackTrace", stackTrace);
        mav.addObject("errorCode", "400");
        mav.setViewName("error");
        return mav;
    }

    @RequestMapping("")
    public String getNotesList(Model model) {

        logger.info("Request for getNotesList()");

        model.addAttribute("notesList", noteService.getAll());

        return "note/list";
    }

    @RequestMapping("/add")
    public String getFormToAdd(Model model) {

        logger.info("Request for getFormToAdd()");

        if (!model.containsAttribute("note"))
            model.addAttribute("note", new Note());

        return "note/add";
    }

    @RequestMapping(value = "/confirm/add", method = RequestMethod.POST)
    public String addNewNote(@Valid @ModelAttribute("note") Note note,
                             BindingResult result,
                             Model model) {

        logger.info("Request for addNewNote()");

        if (result.hasErrors()) {
            model.addAttribute("note", note);
        } else {
            User author = userService.get(note.getUser().getName());
            if (author != null) {
                note.setUser(author);
                noteService.add(note);
                model.addAttribute("successMessage", "Note {" + note.toString() + "} has been successfully added into DB");
            } else {
                model.addAttribute("errorMessage", "User with name {" + note.getUser().getName() + "} not exist. Note creating not possible");
            }
        }

        return getFormToAdd(model);
    }

    @RequestMapping("/remove/{id}")
    public String getFormToRemove(@PathVariable("id") Long noteId,
                             Model model) {

        logger.info("Request for getFormToRemove()");

        Note noteToDelete;
        noteToDelete = noteService.get(noteId);

        if (noteToDelete != null) {
            model.addAttribute("note", noteToDelete);
            return "note/remove";
        } else {
            model.addAttribute("errorMessage", "Note with {ID:" + noteId + "} does not exist");
            return getNotesList(model);
        }
    }

    @RequestMapping("/confirm/remove/{id}")
    public String removeNote(@PathVariable("id") Long noteId,
                             Model model) {

        logger.info("Request for removeNote()");

        Note noteToDelete;
        noteToDelete = noteService.get(noteId);

        if (noteToDelete != null) {
            String deletedNote = noteToDelete.toString();
            noteService.remove(noteId);
            model.addAttribute("successMessage", "Note {" + deletedNote + "} has been successfully removed from DB");
        } else {
            model.addAttribute("errorMessage", "Note with {ID:" + noteId + "} does not exist");
        }

        return getNotesList(model);
    }

    @RequestMapping("/edit/{id}")
    public String getFormToEdit(@PathVariable("id") Long noteId,
                           Model model) {

        logger.info("Request for getFormToEdit()");

        Note noteToEdit;
        noteToEdit = noteService.get(noteId);

        if (noteToEdit != null) {
            model.addAttribute("note", noteToEdit);
            return "note/edit";
        } else {
            model.addAttribute("errorMessage", "Note with {ID:" + noteId + "} does not exist");
            return getNotesList(model);
        }
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updateNote(@Valid @ModelAttribute("note") Note note,
                             BindingResult result,
                             Model model) {

        logger.info("Request for updateNote()");

        if (result.hasErrors()) {
//            return getFormToEdit(note.getId(), model);
            return "note/edit";
        }

        if (note.getId() != null) {
            Note noteToUpdate;
            noteToUpdate = noteService.get(note.getId());

            if (noteToUpdate != null) {
                String lastRecord = noteToUpdate.toString();
                noteService.update(note);
                model.addAttribute("successMessage",
                        "Note information has been successfully updated.<br>" +
                        "Last record: {" + lastRecord + "}<br>" +
                        "New record: {" + note + "}");
                return getNotesList(model);
            }
        }

        model.addAttribute("errorMessage",
                "Note with {ID:" + note.getId() + "} does not exist. Updating is not possible");
        return getNotesList(model);
    }

    @RequestMapping("/search")
    public String getFormToSearch(Model model) {

        logger.info("Request for getFormToSearch()");

        return "note/search";
    }

    @RequestMapping(value = "/search/find", method = RequestMethod.POST)
    public String findNotesByDate(@RequestParam (value = "header", required = false) String header,
                                  @RequestParam(value = "exact_match", required = false) String exactMatch,
                                  @RequestParam("begin") String beginDate,
                                  @RequestParam("end") String endDate,
                                  //BindingResult result,
                                  Model model) {


        logger.info("Request for findNotesByDate()");

        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");

        Date parsedBeginDate;
        Date parsedEndDate;

        try {
            parsedBeginDate = sf.parse(beginDate);
            parsedEndDate = sf.parse(endDate);
        } catch (ParseException exception) {
            model.addAttribute("errorMessage", "Date format is incorrect, expected pattern: YYYY-MM-DD");
            return "note/search";
        }

        List<Note> matches;
        matches = noteService.getByDateAndHeader(parsedBeginDate,
                parsedEndDate,
                header,
                (exactMatch == null) ? false : true);

        if (matches != null && matches.size() > 0) {
            model.addAttribute("notesList", matches);
            model.addAttribute("successMessage", "Found " + matches.size() + " matches");
        } else {
            model.addAttribute("errorMessage", "No matches");
        }

        return "note/list";
    }
}
