package com.tsystems.notesservice.controller;

import com.tsystems.notesservice.domain.User;
import com.tsystems.notesservice.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;


// Method-names in C-style for good understanding

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/root-context.xml", "classpath:spring/servlet-context.xml"})
public class UserConrollerTests {
    @Mock
    UserService userService;

    @InjectMocks
    UserController userController;

    private MockMvc mockMvc;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        mockMvc = standaloneSetup(userController).build();
    }

    @Test
    public void get_users_list() throws Exception {
        User firstUser = new User(1L, "First user", null);
        User secondUser = new User(2L, "Second user", null);

        when(userService.getAll()).thenReturn(Arrays.asList(firstUser, secondUser));

        mockMvc.perform(get("/user/"))
                .andExpect(status().isOk())
                .andExpect(view().name("user/list"))
                .andExpect(forwardedUrl("user/list"))
                .andExpect(model().attribute("usersList", hasSize(2)))
                .andExpect(model().attribute("usersList", hasItem(
                        allOf(
                                hasProperty("id", is(firstUser.getId())),
                                hasProperty("name", is(firstUser.getName()))
                        )
                )))
                .andExpect(model().attribute("usersList", hasItem(
                        allOf(
                                hasProperty("id", is(secondUser.getId())),
                                hasProperty("name", is(secondUser.getName()))
                        )
                )));

        verify(userService, times(1)).getAll();
        verifyNoMoreInteractions(userService);
    }

    @Test
    public void incorrect_query() throws Exception {
        mockMvc.perform(get("/user/edit/{id}", -1L))
                .andExpect(status().isOk())
                .andExpect(view().name("user/list"))
                .andExpect(forwardedUrl("user/list"))
                .andExpect(model().attributeExists("errorMessage"));
    }
}