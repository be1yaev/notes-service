package com.tsystems.notesservice.service;

import com.tsystems.notesservice.domain.User;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

// Method-names in C-style for good understanding

@ContextConfiguration("classpath:spring/root-context.xml")
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional
public class UserServiceTests extends AbstractJUnit4SpringContextTests {
    @Autowired
    protected SessionFactory sessionFactory;

    @Autowired
    protected UserService userService;

    protected User user;

    @Before
    public void init() {
        user = new User(null, "TestUser-Junit", null);

        List<String> names = Arrays.asList("TestUser-Junit", "TestUser-Junit-NewName");

        // Deleting previous data (from last test)
        for (String name : names) {
            User userToDelete = userService.get(name);
            if (userToDelete != null)
                userService.remove(userToDelete.getId());
        }
    }

    @Test
    // All test has been inserted into one to guarantee order of operations:
    // Create -> Read/Update -> Remove
    // (Standardly tests are run parallel)
    public void test_crud_operations() {
        test_create_user();
        test_read_user_by_name();
        test_read_user_by_id();
        test_update_user();
        //test_delete_user();
    }

    public void test_create_user() {
        assertTrue(userService.add(user));
    }

    public void test_read_user_by_name() {
        User readedUser = userService.get(user.getName());

        assertTrue(readedUser != null);

        // Get generated id from inserted user
        user.setId(readedUser.getId());

        assertEquals(
                readedUser.getName(),
                user.getName()
        );
    }

    public void test_read_user_by_id() {
        User readedUser = userService.get(user.getId());

        assertEquals(
                readedUser.getId(),
                user.getId()
        );

        user = readedUser;
    }

    public void test_update_user() {
        String newName;
        newName = "TestUser-Junit-NewName";

        user.setName(newName);

        userService.update(user);

        // Checking changes:
        Query query;
        query = sessionFactory.openSession().createQuery(
                " from User as user where user.name = '" + newName + "'"
        );

        User readedUser;
        readedUser = (User) query.list().get(0);

        assertTrue(readedUser != null);
        assertTrue(readedUser.getId().compareTo(user.getId()) == 0);
    }

    public void test_delete_user() {
        userService.remove(user.getId());
        String userId = "'" + user.getId() + "'";

        Query query;
        query = sessionFactory.openSession().createQuery(
                " from User as user where user.id = " + userId
        );

        assertTrue(query.list().size() == 0);
    }

    @Test(expected = javax.validation.ConstraintViolationException.class)
    public void test_add_user_with_incorrect_data() {
        User incorrectUser = new User(null, "", null);

        userService.add(incorrectUser);
    }
}