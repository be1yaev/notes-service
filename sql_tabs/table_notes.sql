--
-- База данных: `notesservice`
--

-- --------------------------------------------------------

--
-- Структура таблицы `table_notes`
--

CREATE TABLE IF NOT EXISTS `table_notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT 'Username from table_users as PK',
  `header` varchar(50) DEFAULT NULL COMMENT 'Note header',
  `text` varchar(150) NOT NULL COMMENT 'Note text',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Creation date',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Custom notes' AUTO_INCREMENT=31 ;