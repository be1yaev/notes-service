--
-- База данных: `notesservice`
--

-- --------------------------------------------------------

--
-- Структура таблицы `table_users`
--

CREATE TABLE IF NOT EXISTS `table_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT 'Name of user (FIO, maybe)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Зарегистрированные пользователи сервиса' AUTO_INCREMENT=11 ;
